import React from 'react';

export class ListItem extends React.Component {
    render() {
        return (
            <li id={this.props.id}>
                {this.props.value}
                <button type="button" id={this.props.id} onClick={this.props.handleClickDel} value="Delete">Delete</button>
            </li>
        )
    }
}