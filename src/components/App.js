import React from "react";
import "./css/style.css";
import { Form } from "./Form/TodoForm.jsx"
import { List } from "./List/List.jsx"

export class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: [],
            task: {
                id: null,
                value: '',
            },
        }
    }

    handleChange = (event) => {
        this.setState({
            task: {
                id: Date.now(),
                value: event.target.value,
            }
        })
        
    }

    handleSubmit = (event) => {
        event.preventDefault();

        let todos = this.state.todos.slice();
        let task = Object.assign({}, this.state.task);

        todos = [task, ...todos];
        task = {
            id: null,
            value: '',
        };

        this.setState( (state) => {
            return { 
                todos: todos,
                task: task,
            }
        })
        console.log(this.state);
    }

    handleClickDel = (event) => {
        const delId = event.target.id;
        let todos = this.state.todos.slice();
        const delIndex = todos.findIndex(elem => elem.id.toString() === delId);

        todos.splice(delIndex, 1);
        this.setState(state => {
            return {
                todos: todos,
            }
        })
    }

	render() {
        return  (
            <div>
                <Form 
                    value={this.state.task.value} 
                    handleChange={this.handleChange} 
                    handleSubmit={this.handleSubmit}
                />

                <List
                    todos = {this.state.todos}
                    handleClickDel={this.handleClickDel}
                    
                />
            </div>
        )
    }
}