import React from 'react';
export class Form extends React.Component {
    render() {
        return (
            <form onSubmit={this.props.handleSubmit}>
                <input type="text" required value={this.props.value} onChange={this.props.handleChange}/>
                <input type="submit" value="Send"/>
            </form>
        )
    }
}